<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Reloadtransaction;
use App\Red;
use Illuminate\Support\Facades\Log;

class Outgoingcheckmsisdntransaction extends Model {

    protected $table = 'outgoing_checkmsisdn_transaction';

    public static function process($data,$refNo) {
        try {
            
            $checkMsisdn = new Outgoingcheckmsisdntransaction;
            $checkMsisdn->reference_no = $refNo;
            $checkMsisdn->post_data = $data['request'];
            $checkMsisdn->response_data = $data['response'];
            $checkMsisdn->message = $data['message'];
            $checkMsisdn->ret_code = $data['code'];
            $checkMsisdn->save();
            
            return $checkMsisdn->ret_code;
            
        } catch (Exception $ex) {
            return false;
        }
    }

}
