#!/bin/bash

service=schedule

if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
echo "$service is running!!!"
else
echo "service not running"
php -f /usr/share/nginx/html/mobilereload/artisan schedule:run

fi

