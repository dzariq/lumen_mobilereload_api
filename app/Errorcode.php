<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorcode extends Model {

    protected $table = 'error_code';

    public static function getMappedCode($gateway_id, $error_code) {
        $query = Errorcode::select('*')
                ->where('gateway_id', '=', $gateway_id)
                ->where('gateway_error_code', '=', $error_code)
                ->take(1)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query[0];
        }
    }

}
