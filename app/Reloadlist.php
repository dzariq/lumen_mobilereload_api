<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reloadlist extends Model {

    protected $table = 'reload_list';
    protected $primaryKey = 'reload_id';
    protected $fillable = ['provider', 'reload_amount'];

    public static function getReloadList($reload_id) {
        $query = Reloadlist::select('class_name')
                ->leftJoin('provider', 'provider.id', '=', 'reload_list.provider_id')
                ->leftJoin('reload_gw_sequence', 'reload_gw_sequence.provider_id', '=', 'reload_list.provider_id')
                ->leftJoin('gateway', 'gateway.id', '=', 'reload_gw_sequence.gateway_id')
                ->where('reload_id', '=', $reload_id)
                ->where('sort_order', '=', '1')
                ->where('check_msisdn_availability', '=', '1')
                ->take(1)
                ->get();


        if (count($query) == 0) {
            return false;
        } else {
            return $query[0]->class_name;
        }
    }

}
