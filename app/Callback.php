<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Callback extends Model {

    protected $table = 'outgoing_callback_transaction';

    public static function store($callbackUrl,$user_id,$reference_no,$data) {
        try {
            $callback = new Callback;
            $callback->user_id = $user_id;
            $callback->reference_no = $reference_no;
            $callback->callback_url = $callbackUrl;
            $callback->post_data = $data;
            $callback->save();
         
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}
