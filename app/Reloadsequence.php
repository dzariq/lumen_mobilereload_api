<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reloadsequence extends Model {

    protected $table = 'reload_gw_sequence';

    public static function getSequenceNumber($provider_id, $gateway_id) {

        try {
            $query = Reloadsequence::select('*')
                    ->where('provider_id', '=', $provider_id)
                    ->where('gateway_id', '=', $gateway_id)
                    ->limit(1)
                    ->get();

            if (count($query) == 0) {
                return false;
            } else {
                return $query[0]->sort_order;
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public static function getNextGateway($provider_id, $next_sequence) {
        try {
            $query = Reloadsequence::select('*')
                    ->where('provider_id', '=', $provider_id)
                    ->where('sort_number', '=', $next_sequence)
                    ->leftJoin('provider', 'reload_gw_sequence.provider_id', '=', 'provider.id')
                    ->leftJoin('gateway', 'gateway.id', '=', 'reload_gw_sequence.gateway_id')
                    ->limit(1)
                    ->get();

            if (count($query) == 0) {
                return false;
            } else {
                return $query[0];
            }
        } catch (Exception $ex) {
            Log::error('Reload Sequence Model ' . $ex->getMessage());
        }
    }

}
