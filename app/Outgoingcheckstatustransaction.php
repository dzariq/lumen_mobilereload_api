<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Reloadtransaction;
use App\Red;
use Illuminate\Support\Facades\Log;

class Outgoingcheckstatustransaction extends Model {

    protected $table = 'outgoing_check_status_transaction';

    public static function store($refNum, $xml_post_string, $response, $flag, $message) {
        try {
            Log::info('Ougoing check status: ' .$refNum);

            $store = new Outgoingcheckstatustransaction();
            $store->reference_no = $refNum;
            $store->post_data = $xml_post_string;
            $store->response_data = $response;
            $store->message = $message;
            $store->save();

            $reloadTxn = Reloadtransaction::where('internal_ref_no', '=', $refNum)
                    ->get();
            $reloadTxn = $reloadTxn[0];
            if ($flag == 0) {
                //do callback to client
                //update status success
                $reloadTxn->message = $message;
                $reloadTxn->status = 0;
                $reloadTxn->save();
                app('App\Http\Controllers\CallbackController')->index($refNum);
                //   Request::create('mobilereload/callback/'.$refNum, 'GET', array($refNum));
                //redirect('mobilereload/callback/' . $refNum);
            } else if ($flag == 2) {
                //do callback to client
                //update status failed
                $reloadTxn->message = $message;
                $reloadTxn->status = 2;
                $reloadTxn->save();
                app('App\Http\Controllers\CallbackController')->index($refNum);
            } else {
                //do nothing
            }
        } catch (Exception $ex) {
            Log::error('Outgoing check reload txn error: ' . $ex->getMessage());
        }
    }

}
