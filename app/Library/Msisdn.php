<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;
use App\Outgoingreloadtransaction;
use App\Outgoingcheckstatustransaction;

class Msisdn {

    public function checkMobileNumber($mobile_number) {
        $soapUrl = env('CELCOM_URL', ''); // asmx URL of WSDL

        $xml_post_string = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidAccountQuery xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:svcNum>" . $mobile_number . "</ns1:svcNum>
</ns1:PrepaidAccountQuery>
</soapenv:Body>
</soapenv:Envelope>";

        $headers = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidAccountQuery",
            "Content-Length: " . strlen($xml_post_string)
        ); //SOAPAction: your op URL
// PHP cURL  for https connection with auth
        $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, "https://202.188.114.29/TestPG");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// converting
        $response = curl_exec($ch);
        curl_close($ch);


        $response = str_replace("soapenv:", "", $response);
        $response = str_replace("ns1:", "", $response);
        $statusQuery = simplexml_load_string($response);

        $retCode = $statusQuery->Body->PrepaidAccountResponse->retCode;
        $message = '';

        $returnJson = array(
            'request' => $xml_post_string,
            'message' => $message,
            'response' => $response,
            'code' => $retCode
        );

        return $returnJson;
    }

}

?>