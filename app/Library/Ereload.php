<?php

namespace App\Library;

use SoapClient;
use Illuminate\Support\Facades\Log;
use App\Outgoingreloadtransaction;
use App\Outgoingcheckstatustransaction;

class Ereload {

    private $boolean = true;
    private $sessionToken;

    public function reload($gateway_id, $mobile_number, $amount, $provider, $provider_id, $refNum, $pendingTxns) {
        $retCode = NULL;
        $message = '';

        $params = array(
            'sClientUserName' => env('ERELOAD_LOGIN', ''),
            'sClientPassword' => env('ERELOAD_PASS', ''),
            'sClientTxID' => ($refNum),
            'sProductID' => $pendingTxns->ereload_id,
            'dProductPrice' => $amount,
            'sCustomerAccountNumber' => $mobile_number,
            'sCustomerMobileNumber' => $mobile_number,
            'sDealerMobileNumber' => '',
            'sRemark' => '',
            'sOtherParameter' => '',
            'sResultID' => '',
            'sResultStatus' => '',
        );
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('ERELOAD_URL', ''));
        $res = $client->__soapCall('eReload', array($params));

        Log::info('Outgoing ERELOAD Reload Request: ' . json_encode($params));
        Log::info('Outgoing ERELOAD Reload Response: ' . json_encode($res));

//        if ($statusReload->Body->PrepaidTopupResponse->retCode != 0) {
//            // $this->failed_reload($refNum, "Error: Code: " . $statusReload->Body->PrepaidTopupResponse->retCode . ": " . $statusReload->Body->PrepaidTopupResponse->retMsg);
//            $message = $statusReload->Body->PrepaidTopupResponse->retMsg;
//            $retCode = $statusReload->Body->PrepaidTopupResponse->retCode;
//        }


        Outgoingreloadtransaction::store($gateway_id, $refNum, serialize($params), serialize($res), $message, $refNum, $retCode, $provider_id);
    }

    public function check($refNum, $dateFrom, $dateTo,$mobile_no) {
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('ERELOAD_URL', ''));
        $params = array(
            'sClientUserName' => env('ERELOAD_LOGIN', ''),
            'sClientPassword' => env('ERELOAD_PASS', ''),
            'sClientTxID' => ($refNum),
            'sCustomerAccountNumber' => $mobile_no,
            'sCustomerMobileNumber' => $mobile_no,
        );
        var_dump($params);
        $res = $client->__soapCall('checkTrans', array($params));
        var_dump($res);
        $this->log('Ereload Check status Request: ', $params);
        $this->log('Ereload Check status Response: ', $res);
        $message = 'REF:' . $res->sTransactionDNReceivedID;
        $flag = 1; //pending
        if ($res->sTransactionStatus == 'SUCCESS') {
          //  $this->success_reload($refNum, $message);
            $flag = 0; //success
        } else if ($res->sTransactionStatus == 'COMPLETED') {
            
        } else if ($res->sTransactionStatus == 'PENDING') {
            
        } else {
          //  $this->failed_reload($refNum, $message);
            $flag = 2; //failed
        }

        Outgoingcheckstatustransaction::store($refNum, serialize($params), serialize($res), $flag, $message);
    }

    public function convertoXML($res) {
        // converting
        $res = str_replace("<soap:Body>", "", $res);
        $res = str_replace("</soap:Body>", "", $res);

        // convertingc to XML
        $parser = simplexml_load_string($res);
        return $res;
    }

}
