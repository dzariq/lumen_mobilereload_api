<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;
use App\Outgoingreloadtransaction;
use App\Outgoingcheckstatustransaction;

class Celcom {

    public function checkMobileNumber($mobile_number) {
        $soapUrl = env('CELCOM_URL', ''); // asmx URL of WSDL

        $xml_post_string = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidAccountQuery xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:svcNum>" . $mobile_number . "</ns1:svcNum>
</ns1:PrepaidAccountQuery>
</soapenv:Body>
</soapenv:Envelope>";

        $headers = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidAccountQuery",
            "Content-Length: " . strlen($xml_post_string)
        ); //SOAPAction: your op URL
// PHP cURL  for https connection with auth
        $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, "https://202.188.114.29/TestPG");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// converting
        $response = curl_exec($ch);
        curl_close($ch);


        $response = str_replace("soapenv:", "", $response);
        $response = str_replace("ns1:", "", $response);
        $statusQuery = simplexml_load_string($response);

        $retCode = $statusQuery->Body->PrepaidAccountResponse->retCode;
        $message = '';

        $returnJson = array(
            'request' => $xml_post_string,
            'message' => $message,
            'response' => $response,
            'code' => $retCode
        );

        return $returnJson;
    }

    public function reload($gateway_id, $mobile_number, $amount, $provider, $provider_id, $refNum) {
        $retCode = NULL;
        $message = '';

        //check mobile number is celcom number
        //  $this->checkMobileNumber($gateway_id, $mobile_number, $refNum);

        $soapUrl = env('CELCOM_URL', ''); // asmx URL of WSDL
        $dateTxn = date('Y-m-d') . "T" . date('H:i:s');

        //data reload e.g CEL-5-DATA
//        if (isset($temp[2]) && $temp[2] == 'DATA') {
//            $params = $this->reloadDataParams($refNum, $mobile_number, $amount, $dateTxn);
//        } else {
        $params = $this->reloadAirtimeParams($refNum, $mobile_number, $amount, $dateTxn);
//        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params['xml_post_string']); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);


// converting
        $response = curl_exec($ch);


        Log::info('Outgoing CELCOM Reload Request: ' . json_encode($params['xml_post_string']));
        Log::info('Outgoing CELCOM Reload Response: ' . json_encode($response));

        $statusReload = str_replace("soapenv:", "", $response);
        $statusReload = str_replace("ns1:", "", $statusReload);
        $statusReload = simplexml_load_string($statusReload);

        curl_close($ch);

        if ($statusReload->Body->PrepaidTopupResponse->retCode != 0) {
            // $this->failed_reload($refNum, "Error: Code: " . $statusReload->Body->PrepaidTopupResponse->retCode . ": " . $statusReload->Body->PrepaidTopupResponse->retMsg);
            $message = $statusReload->Body->PrepaidTopupResponse->retMsg;
            $retCode = $statusReload->Body->PrepaidTopupResponse->retCode;
        }


        Outgoingreloadtransaction::store($gateway_id, $refNum, serialize($params), serialize($response), $message, $refNum, $retCode, $provider_id);

        return 'CELCOM Reload Initiated :: ' . $dateTxn;
    }

    public function check($refNum) {
        $url = env('CELCOM_URL', '');
        $soapUrl = $url; // asmx URL of WSDL
        //data reload e.g CEL-5-DATA
//        if (isset($temp[2]) && $temp[2] == 'DATA') {
//            $params = $this->checkDataParams($bill['order_id']);
//        } else {
        $params = $this->checkAirtimeParams($refNum);
//        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params['xml_post_string']); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);


        $response = curl_exec($ch);
        curl_close($ch);


        Log::info('Outgoing CELCOM Check Request: ' . json_encode($params['xml_post_string']));
        Log::info('Outgoing CELCOM Check Response: ' . json_encode($response));

        $statusCheck = str_replace("soapenv:", "", $response);
        $statusCheck = str_replace("ns1:", "", $statusCheck);
        $statusCheck = simplexml_load_string($statusCheck);


        if ($statusCheck->Body->PrepaidTopupResponse->retCode != 0) {
            $flag = 2;
        } else if ($statusCheck->Body->PrepaidTopupResponse->retCode == 0) {
            $flag = 0;
        } else {
            $flag = 1;
        }
        $message = $statusCheck->Body->PrepaidTopupResponse->retMsg;

        Outgoingcheckstatustransaction::store($refNum, $params['xml_post_string'], $response, $flag, $message);
    }

    public function recon($bills) {
//        create recon string
        $reconString = "";
        foreach ($bills as $bill) {
            $temp = explode('-', $bill['amount']);
            $amount = $temp[1] * 100;
            $txnType = "0";

            //data reload e.g CEL-5-DATA
            if (isset($temp[2]) && $temp[2] == 'DATA') {
                $code = '209';
            } else {
                $code = '201';
            }

            $temp = explode('::', $bill['request']);
            $rawDate = $temp[1];
            $temp2 = explode('T', $rawDate);
            $date = str_replace('-', '', $temp2[0]);
            $time = str_replace(':', '', $temp2[1]);

            $txnDate = trim(str_replace("\"", "", $date . $time));


            $reconString .= $code;
            $reconString .= "|";
            $reconString .= $bill['order_id'];
            $reconString .= "|";
            $reconString .= $bill['mobile_no'];
            $reconString .= "|";
            $reconString .= $amount;
            $reconString .= "|";
            $reconString .= $txnDate;
            $reconString .= "|";
            $reconString .= $txnType;
            $reconString .= "|";
            $reconString .= "\n";
        }

        $reconString .= "TOT. REC : " . count($bills);

//echo $reconString;die;
        $url = env('CELCOM_URL', '');
        $soapUrl = $url; // asmx URL of WSDL
        $xml_post_string = "<?xml version='1.0' encoding='UTF-8'?>
    <soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
    <soapenv:Body>
    <ns1:ReconReport xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
    <ns1:filename>" . date('Ymd', strtotime("-1 days")) . ".txt</ns1:filename>
    <ns1:desc>Client ReconReport</ns1:desc>
    <ns1:attachment>
    " . base64_encode($reconString) . "
</ns1:attachment>
</ns1:ReconReport>
</soapenv:Body>
</soapenv:Envelope>";
        $headers = array(
            "Content-Type: application/soap+xml,
        Transfer-Encoding: chunked,
        action=urn:ReconReport,
        Content-Length:" . strlen($xml_post_string)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $response = curl_exec($ch);
        curl_close($ch);

        Log::info('Outgoing CELCOM Recon Request: ' . json_encode($xml_post_string));
        Log::info('Outgoing CELCOM Recon Response: ' . json_encode($response));
    }

    public function log($item, $data) {
        $tag = $item . serialize($data);
        $myfile = fopen(__DIR__ . "/celcomlog.txt", "a") or die("Unable to open file!");
        $txt = date('Y-m-d H:i:s') . " $tag\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

    public function reloadDataParams($refNum, $mobile_number, $amount, $dateTxn) {
        $data = array();
        $data['xml_post_string'] = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidReloadMobileInternetRequest xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:clientRefId>" . $refNum . "</ns1:clientRefId>
<ns1:svcNum>" . $mobile_number . "</ns1:svcNum>
<ns1:amount>" . $amount . "</ns1:amount>
<ns1:transDate>" . $dateTxn . ".000+08:00</ns1:transDate>
<ns1:transType>0</ns1:transType>
</ns1:PrepaidReloadMobileInternetRequest>
</soapenv:Body>
</soapenv:Envelope>";

        $data['headers'] = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidReloadMobileInternetRequest",
            "Content-Length: " . strlen($data['xml_post_string'])
        ); //SOAPAction: your op URL

        return $data;
    }

    public function reloadAirtimeParams($refNum, $mobile_number, $amount, $dateTxn) {
        $data = array();

        $data['xml_post_string'] = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidCashTopup xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:clientRefId>" . $refNum . "</ns1:clientRefId>
<ns1:svcNum>" . $mobile_number . "</ns1:svcNum>
<ns1:amount>" . $amount * 100 . "</ns1:amount>
<ns1:transDate>" . $dateTxn . ".000+08:00</ns1:transDate>
<ns1:transType>0</ns1:transType>
</ns1:PrepaidCashTopup>
</soapenv:Body>
</soapenv:Envelope>";

        $data['headers'] = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidCashTopup",
            "Content-Length: " . strlen($data['xml_post_string'])
        ); //SOAPAction: your op URL
        return $data;
    }

    public function checkDataParams($order_id) {
        $data = array();

        $data['xml_post_string'] = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidQueryReloadMobileInternetRequest xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:clientRefId>" . $order_id . "</ns1:clientRefId>
</ns1:PrepaidQueryReloadMobileInternetRequest>
</soapenv:Body>
</soapenv:Envelope>";

        $data['headers'] = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidQueryReloadMobileInternetRequest",
            "Content-Length: " . strlen($data['xml_post_string'])
        );

        return $data;
    }

    public function checkAirtimeParams($order_id) {
        $data = array();

        $data['xml_post_string'] = "<?xml version='1.0' encoding='UTF-8'?>
<soapenv:Envelope xmlns:soapenv='http://www.w3.org/2003/05/soap-envelope'>
<soapenv:Body>
<ns1:PrepaidTopupQuery xmlns:ns1='http://paymentGateway.celcom.com'>
<ns1:login>" . env('CELCOM_LOGIN', '') . "</ns1:login>
<ns1:pass>" . env('CELCOM_PASS', '') . "</ns1:pass>
<ns1:clientRefId>" . $order_id . "</ns1:clientRefId>
</ns1:PrepaidTopupQuery>
</soapenv:Body>
</soapenv:Envelope>";

        $data['headers'] = array(
            "Content-Type: application/soap+xml",
            "charset: UTF-8",
            "action: urn:PrepaidTopupQuery",
            "Content-Length: " . strlen($data['xml_post_string'])
        );

        return $data;
    }

}
