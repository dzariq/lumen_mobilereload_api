<?php

namespace App\Library;

use SoapClient;
use Illuminate\Support\Facades\Log;
use App\Outgoingreloadtransaction;
use App\Outgoingcheckstatustransaction;

class Mypay {

    private $boolean = true;
    private $sessionToken;

    public function auth() {
        /**
         * <header>
          <authentication>
          <terminalId>60123456789</terminalId>
          <authKey>1234</authKey>
          </authentication>
          <agentVersion>scp-1.4.7.0_S966</agentVersion>
          <agentTransactionId>119110</agentTransactionId>
          <agentTerminalId>1</agentTerminalId>
          <agentTimeStamp>2016-03-10T13:21:44.308+08:00</agentTimeStamp>
          <languageCode>en</languageCode>
          </header>
         */
        $params = array(
            "header" => array(
                "authentication" => array(
                    "terminalId" => env('MYPAY_TERMINAL_ID', ''),
                    "authKey" => env('MYPAY_AUTH_KEY', ''),
                ),
                "agentVersion" => env('MYPAY_AGENT_VERSION', ''),
                "agentTransactionId" => env('MYPAY_AGENT_TXN_ID', ''),
                "agentTerminalId" => env('MYPAY_TERMINAL_ID', ''),
                "agentTimeStamp" => '' . date('Y-m-d') . 'T' . date('H:i:s') . '.000+08:00',
                "languageCode" => env('MYPAY_LANGUAGE', ''),
            )
        );
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('MYPAY_URL_AGENT', ''));
        //  		var_dump($client->__getFunctions());
        //  		var_dump($client->__getTypes());
        $res = $client->__soapCall("AgentLogin", array($params));
// 		var_dump($params);
// 		var_dump($res);
        if ($res->sessionToken != null)
            $this->sessionToken = $res->sessionToken;
// 		if($this->boolean)echo $this->sessionToken;
        return $this->sessionToken != null;
    }

    public function resolveMsisdn($msisdn) {
        $params = array(
            'header' => array(
                'authentication' => array(
                    'sessionToken' => $this->sessionToken
                ),
                'agentTransactionId' => env('MYPAY_AGENT_TXN_ID', ''),
                'agentTimeStamp' => env('MYPAY_AGENT_TIMESTAMP', ''),
            ),
            'msisdn' => $msisdn,
//     		'keyValues'						=> null,
        );
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('MYPAY_URL_TOPUP', ''));
        $res = $client->__soapCall('ResolveMsisdn', array($params));
      var_dump($params);
        var_dump($res);die;
    }

    public function reload($gateway_id, $mobile_number, $amount, $provider, $provider_id, $refNum) {

        $this->auth();
     	//$this->resolveMsisdn($mobile_number);

        if (!is_numeric($amount)) {
            $temp = explode('-', $amount);
            $amount = $temp[1];
            $provider = $temp[0];
        }

        $phone = $mobile_number;
        if (strpos(' ' . $mobile_number, '6') != 1) {
            $mobile_number = '6' . $mobile_number;
        } else {
            $phone = ltrim($phone, '6');
        }

        $params = array(
            'header' => array(
                'authentication' => array(
                    'sessionToken' => $this->sessionToken
                ),
                'agentTransactionId' => env('MYPAY_AGENT_TXN_ID', ''),
                'agentTimeStamp' => '' . date('Y-m-d') . 'T' . date('H:i:s') . '.000+08:00',
            ),
            'msisdn' => $mobile_number,
            'productCode' => $provider,
            'value' => $amount,
            'extraProducts' => array(
                'code' => env('MYPAY_AGENT_CODE', '')
            )
        );
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('MYPAY_URL_TOPUP', ''));
        $message = '';
        $res = $client->__soapCall('MakeSubscriberTopup', array($params));
        //var_dump($params);
        // var_dump($res);
        //	$message='REF:'.$res->header->transactionId.' BAL:'.$res->wallets->wallet->balance.' - '.$res->header->resultDescription;
        if (isset($res) && $res->header->resultCode != '0') {
            $message = 'REF:' . $res->header->transactionId . ' - ' . $res->header->resultDescription;
            $txnId = $res->header->transactionId;
        } else if (isset($res) && $res->header->resultCode == '0') {
            $message = 'REF:' . $res->header->transactionId . ' BAL:' . $res->wallets->wallet->balance . ' - ' . $res->header->resultDescription;
            $txnId = $res->header->transactionId;
        } else {
            $res = '';
            $txnId = '';
        }

        Log::info('Outgoing MYPAY Reload Request: ' . json_encode($params));
        Log::info('Outgoing MYPAY Reload Response: ' . json_encode($res));
        Outgoingreloadtransaction::store($gateway_id, $refNum, serialize($params), serialize($res), $message, $txnId, NULL, $provider_id);
    }

    public function check($refNum, $dateFrom, $dateTo) {
        $this->auth();

        $params = array(
            'header' => array(
                'authentication' => array(
                    'sessionToken' => $this->sessionToken
                ),
                "agentVersion" => env('MYPAY_AGENT_VERSION', ''),
                "agentTransactionId" => env('MYPAY_AGENT_TXN_ID', ''),
                "agentTerminalId" => env('MYPAY_TERMINAL_ID', ''),
                "agentTimeStamp" => '' . date('Y-m-d') . 'T' . date('H:i:s') . '.000+08:00',
                "languageCode" => env('MYPAY_LANGUAGE', ''),
            ),
            'dateFrom' => $dateFrom . 'T00:00:00.000+08:00',
            'dateTo' => $dateTo . 'T00:00:00.000+08:00',
            'maxRecords' => '100',
            'transReference' => $refNum,
            'transTypesFilter' => 'Sale:Rech:Ref:Rev:Rdem',
            'includeSubAgents' => true,
//     		'keyValues'						=> null,
        );
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient(env('MYPAY_URL_REPORT', ''));
        $res = $client->__soapCall('GetTransactionInfo', array($params));
        // var_dump($res);

        $message = '';
        $flag = 1;
        if ($res->transactions) {
            foreach ($res->transactions as $transaction) {
                if ($transaction->reference == $refNum) {
                    if ($transaction->isRefunded == true) {
                        $message = 'Refunded';
                        $flag = 2;
                    }
                }
            }
        }

        Log::info('Outgoing MYPAY Check Request: ' . json_encode($params));
        Log::info('Outgoing MYPAY Check Response: ' . json_encode($res));

        Outgoingcheckstatustransaction::store($refNum, serialize($params), serialize($res), $flag, $message);
    }

    public function convertoXML($res) {
        // converting
        $res = str_replace("<soap:Body>", "", $res);
        $res = str_replace("</soap:Body>", "", $res);

        // convertingc to XML
        $parser = simplexml_load_string($res);
        return $res;
    }

}
