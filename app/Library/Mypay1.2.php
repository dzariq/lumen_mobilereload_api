<?php

class Mypay extends Reload {

	private $boolean = true;
	private $sessionToken;
	
	public function auth() {
		/**
		 * <header>
		        <authentication>
		          <terminalId>60123456789</terminalId>
		          <authKey>1234</authKey>
		        </authentication>
		        <agentVersion>scp-1.4.7.0_S966</agentVersion>
		        <agentTransactionId>119110</agentTransactionId>
		        <agentTerminalId>1</agentTerminalId>
		        <agentTimeStamp>2016-03-10T13:21:44.308+08:00</agentTimeStamp>
		        <languageCode>en</languageCode>
		      </header>
		 */
		$params = array(
			"header" =>  array(
				"authentication" 		=> array(
						"terminalId"		=> $this->config->mypay->terminalId,
						"authKey"			=> $this->config->mypay->authKey
				),
				"agentVersion" 			=> $this->config->mypay->agentVersion,
				"agentTransactionId" 	=> $this->config->mypay->agentTransactionId,
				"agentTerminalId" 		=> $this->config->mypay->agentTerminalId,
				"agentTimeStamp" 		=> ''.date('Y-m-d').'T'.date('H:i:s').'.000+08:00',
				"languageCode"			=> $this->config->mypay->languageCode
			)
		);
		ini_set("soap.wsdl_cache_enabled", "0");
		$client = new SoapClient($this->config->mypay->url_agent);
		//  		var_dump($client->__getFunctions());
		//  		var_dump($client->__getTypes());
		$res = $client->__soapCall("AgentLogin", array($params));
// 		var_dump($params);
// 		var_dump($res);
		if($res->sessionToken != null) $this->sessionToken = $res->sessionToken;
// 		if($this->boolean)echo $this->sessionToken;
		return $this->sessionToken!=null;
	}
	
	public function resolveMsisdn($msisdn) {
		$params = array(
			'header' => array(
				'authentication' 		=> array(
						'sessionToken' 	=> $this->sessionToken
				),
				'agentTransactionId' 	=> $this->config->mypay->agentTransactionId,
				'agentTimeStamp'		=> $this->config->mypay->agentTimeStamp,
			),
    		'msisdn'						=> $msisdn,
//     		'keyValues'						=> null,
    	);
    	ini_set("soap.wsdl_cache_enabled", "0");
    	$client = new SoapClient($this->config->mypay->url_topup);
    	$res = $client->__soapCall('ResolveMsisdn', array($params));
    	var_dump($params);
    	var_dump($res);
	}

    public function reload($mobile_number, $amount, $provider, $refNum) {
    	
//     	$this->resolveMsisdn($mobile_number);
    	
    	if (!is_numeric($amount)) {
    		$temp = explode('-', $amount);
    		$amount = $temp[1];
    		$provider = $temp[0];
    	}
    	
    	/**
    	 * <header>
			<authentication>
				<sessionToken>96D5D1C92085F4A3E0F55069</sessionToken>
			</authentication>
			<agentTransactionId>139371</agentTransactionId>
			<agentTimeStamp>2015-11-12T15:11:53.947+08:00</agentTimeStamp>
			</header>
			<msisdn>0812345679</msisdn>
			<productCode>AST</productCode>
			<value>10.0</value>
    	 */
		$phone=$mobile_number;
		if(strpos(' '.$mobile_number,'6')!=1){
			$mobile_number='6'.$mobile_number;
		}else{
			$phone=ltrim($phone, '6');
		}
    	$params = array(
			'header' => array(
				'authentication' => array(
					'sessionToken' 	=> $this->sessionToken
				),
				'agentTransactionId' 	=> $this->config->mypay->agentTransactionId,
				'agentTimeStamp'		=> ''.date('Y-m-d').'T'.date('H:i:s').'.000+08:00',
			),
    		'msisdn'						=> $mobile_number,
    		'productCode'					=> $provider,
    		'value'							=> $amount
    	);
    	ini_set("soap.wsdl_cache_enabled", "0");
    	$client = new SoapClient($this->config->mypay->url_topup);
    	$res = $client->__soapCall('MakeSubscriberTopup', array($params));
    	//var_dump($params);
     	var_dump($res);
    	$this->log('Reload Request: ', $params);
    	$this->log('Reload Response: ', $res);
  		$result =$this->db->query("SELECT * FROM reload WHERE mobile_no like '%$phone' and amount='$provider-$amount' and status='pending' limit 1");
    	$row = $result->fetchArray();
    	
    //	$message='REF:'.$res->header->transactionId.' BAL:'.$res->wallets->wallet->balance.' - '.$res->header->resultDescription;
    	
    	if ($res->header->resultCode!='0') {
		$message='REF:'.$res->header->transactionId.' - '.$res->header->resultDescription;
    		$this->failed_reload($row['order_id'], $message);
    	} else if ($res->header->resultCode=='0') {
		$message='REF:'.$res->header->transactionId.' BAL:'.$res->wallets->wallet->balance.' - '.$res->header->resultDescription;
    		$this->success_reload($row['order_id'], $message);
    	}
    	return $message;
    }

    public function check($bill) {
    	$this->failed_reload($bill['order_id'], 'CS refund');
    }
    
    public function convertoXML($res) {
    	// converting
    	$res = str_replace("<soap:Body>", "", $res);
    	$res = str_replace("</soap:Body>", "", $res);
    	
    	// convertingc to XML
    	$parser = simplexml_load_string($res);
    	return $res;
    }

    public function log($item, $data) {
        $tag = $item . serialize($data);
        $myfile = fopen(__DIR__ . "/mypaylog.txt", "a") or die("Unable to open file!");
        $txt = date('Y-m-d H:i:s') . " $tag\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

}
