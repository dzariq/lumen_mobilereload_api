<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Checkstatustransaction extends Model {

    protected $table = 'incoming_check_status_transaction';

    public static function store($user_id,$data,$error_code,$ip) {
        try {
            $checkStatus = new Checkstatustransaction;
            $checkStatus->ip_address = $ip;
            $checkStatus->reference_no = $data['reference_no'];
            $checkStatus->status = $data['status'];
            $checkStatus->message = $data['message'];
            $checkStatus->user_id = $user_id;
            $checkStatus->error_code = $error_code;
            $checkStatus->save();
         
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}
