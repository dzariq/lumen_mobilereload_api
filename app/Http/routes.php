<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->routeMiddleware([
    'hmac' => App\Http\Middleware\Hmac::class
]);

$app->group(['middleware' => 'hmac','namespace' => 'App\Http\Controllers'], function ($app) {
    $app->get('mobilereload/reloadlist', 'ReloadlistController@index');
    $app->post('mobilereload/reload', 'ReloadController@index');
    $app->post('mobilereload/checkstatus', 'CheckstatusController@index');
    $app->post('mobilereload/checkmsisdn', 'CheckmsisdnController@index');
    $app->post('mobilereload/testreceivecallback', 'TestcallbackController@index');
});
//cron
$app->get('mobilereload/dummycheckstatus', 'DummycheckstatusController@index');
$app->get('mobilereload/runreload', 'CronrunreloadController@index');
//$app->get('mobilereload/callback/{reference_no}', 'CallbackController@index');
//$app->get('mobilereload/runreload', 'RunreloadController@index');
$app->get('mobilereload/runcheckstatus', 'CronruncheckstatusController@index');
