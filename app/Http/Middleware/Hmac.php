<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use DB;
use App\User;
use Log;

class Hmac {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        Log::info('API Request ' . $request->path() . ': ' . json_encode($request->input()));

        if (!$request->headers->has('X-Authentication')) {
            return response('Unauthorized.', 401);
        }
        $xAuthentication = $request->header('X-Authentication');

        //get username
        $dataHeader = $this->extract($xAuthentication);
        if (!$dataHeader) {
            return response('Unauthorized.', 401);
        }
        //header data
        $username = $dataHeader['username'];
        $request_hash = $dataHeader['hash'];
        $path = str_replace('mobilereload', '', $request->path());
        $method = $request->method();
        //header data
        //comparing request_hash with generated_hash
        //get password by username
        $user = User::where('username', '=', $username)->get();

        if (!$user) {
            return response('Unauthorized.', 401);
        }

        $password = $user[0]->password;

        $hash = $this->createHash($username, $password, $path, $method);


        if (strcmp($request_hash, $hash) != 0) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }

    private function extract($xAuthentication) {
        $temp = explode(':', $xAuthentication);
        if (count($temp) < 2) {
            return false;
        }

        return array('username' => $temp[0], 'hash' => $temp[1]);
    }

    private function createHash($username, $password, $path, $method) {
        $secretKey = $password . $username;

        return hash_hmac(
                "sha256", $path . $method, $secretKey
        );
    }

}
