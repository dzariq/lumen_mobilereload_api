<?php

namespace App\Http\Controllers;

use App\Callback;
use App\Reloadtransaction;
use App\User;
use DB;
use Log;
use Symfony\Component\Console\Input\Input;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CallbackController extends Controller {

    public function index($reference_no) {
        try {
            //get callback url
            $callbackUrl = Reloadtransaction::getCallbackUrl($reference_no);
            $user_id = Reloadtransaction::getUserId($reference_no);
            //post data to url
            //get txn data
            $txnData = Reloadtransaction::getTxn($reference_no);
            //post data to url
            $fields = array(
                'status' => urlencode($txnData->status),
                'message' => urlencode($txnData->message),
                'mobile_no' => urlencode($txnData->mobile_no),
                'datetime' => urlencode($txnData->tx_datetime),
                'reference_no' => urlencode($txnData->reference_no),
            );

            $fields_string = '';

//url-ify the data for the POST
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $callbackUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//   curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string); // the SOAP request
// converting
            $response = curl_exec($ch);

            curl_close($ch);

            Callback::store($callbackUrl, $user_id, $reference_no, $fields_string);
            Log::info('Callback Sent ' . json_encode($fields_string));
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

}
