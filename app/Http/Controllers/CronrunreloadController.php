<?php

namespace App\Http\Controllers;

use App\Checkstatustransaction;
use App\Reloadtransaction;
use App\User;
use App\Errorcode;
use DB;
use Log;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CronrunreloadController extends Controller {

    public function index(Request $request) {

        try {
//get pending txns
            $pendingTxnsall = Reloadtransaction::getPendingTxns(20);
            //Log::info('PENDING TXNS: ',($pendingTxnsall));

            if ($pendingTxnsall) {
                foreach ($pendingTxnsall as $pendingTxns) {
//route to celcom, mypay
                    $reload_class = '\\App\\Library\\' . $pendingTxns->class_name;
                    $class = new $reload_class();
                    Log::info('Run Reload ');
                    $class->reload($pendingTxns->gateway_id, $pendingTxns->mobile_no, $pendingTxns->reload_amount, $pendingTxns->provider_code, $pendingTxns->provider_id, $pendingTxns->reference_no, $pendingTxns);
                }
            }
        } catch (Exception $ex) {
            Log::error('Run Reload ' . $ex->getMessage());
        }
    }

}
