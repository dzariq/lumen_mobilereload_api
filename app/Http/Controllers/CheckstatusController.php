<?php

namespace App\Http\Controllers;

use App\Checkstatustransaction;
use App\Reloadtransaction;
use App\User;
use App\Errorcode;
use DB;
use Log;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckstatusController extends Controller {

    public function index(Request $request) {
        try {
            $this->validate($request, [
                'reference_no' => 'required',
            ]);

            $error_code = '';
            $message = '';


            //check reload_id valid
            $reloadTxn = Reloadtransaction::where('reference_no', '=', $request->input('reference_no'))
                    ->get();


            if (count($reloadTxn) == 0) {
                $error_code = 10;
                $errorCodes = Errorcode::where('custom_error_code', '=', $error_code)
                        ->select('description')
                        ->get();
                $message = $errorCodes[0]->description;
                $status = 2;
                $mobile_no = '';
            } else {
                $status = $reloadTxn[0]->status;
                $mobile_no = $reloadTxn[0]->mobile_no;
                $message = $reloadTxn[0]->message;
                $error_code = $reloadTxn[0]->error_code;
            }


            $response = [
                'status' => $status,
                'message' => $message,
                'mobile_no' => $mobile_no,
                'datetime' => date('Y-m-d H:i:s'),
                'reference_no' => $request->input('reference_no')
            ];

            $user = User::getUserByHeader($request->header('X-Authentication'));

            //store check_status table
            Checkstatustransaction::store($user->id, $response, $error_code, $request->ip());

            Log::info('API Response ' . $request->path() . ': ' . json_encode($response));

            return response()->json($response);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

}
