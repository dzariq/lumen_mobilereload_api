<?php

namespace App\Http\Controllers;

use Log;
use Symfony\Component\Console\Input\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestcallbackController extends Controller {

    public function index(Request $request) {
        echo 'ok';
        Log::info('Callback Received '. json_encode($request->input()));
    }

}
