<?php

namespace App\Http\Controllers;

use App\Reloadtransaction;
use App\User;
use App\Errorcode;
use DB;
use Log;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReloadController extends Controller {

    public function index(Request $request) {
        try {
            $this->validate($request, [
                'tx_id' => 'required',
                'mobile_no' => 'required',
                'reload_id' => 'required',
                'tx_datetime' => 'required',
            ]);

            $e_message = '';
            $status = 1;
            $refNo = '';

            //=============validation=============
            //check reload_id valid
            $reloadPackage = Reloadlist::find($request->input('reload_id'));
            if (empty($reloadPackage)) {
                $e_message = 'Invalid Reload Id';
            } else {

                //check credit balance
                $details = $this->checkBalance($request->header('X-Authentication'), $reloadPackage->reload_amount);
                if (!$details['status']) {
                    $e_message = 'Insufficient Balance';
                }
                //=============validation=============
//success condition
                //insert db transaction & deduct balance
                $refNo = $random = 'MR_' . $details['user_id'] . time() . rand(10 * 45, 100 * 98);
                if ($e_message == '') {
                    $status = 0;
                    $e_message = 'success';
                }
                Reloadtransaction::processTransaction($request->input(), $refNo, $details, $request->ip());
//success condition
            }
            $response = [
                'status' => $status,
                'message' => $e_message,
                'tx_id' => $request->input('tx_id'),
                'datetime' => date('Y-m-d H:i:s'),
                'reference_no' => $refNo
            ];

            Log::info('API Response ' . $request->path() . ': ' . json_encode($response));

            return response()->json($response);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function checkBalance($xAuthentication, $amount) {
        //

        $status = true;
        $message = 'reload initiated';
        $error_code = '';

        $user = User::getUserByHeader($xAuthentication);

        if ($user->balance < $amount) {
            $status = false;
            $error_code = '08';
            $errorCodes = Errorcode::where('custom_error_code', '=', $error_code)
                    ->select('description')
                    ->get();
            $message = $errorCodes[0]->description;
        }

        $details = array(
            'amount' => $amount,
            'user_balance' => $user->balance,
            'user_id' => $user->id,
            'status' => $status,
            'error_code' => $error_code,
            'message' => $message
        );

        //MR_{userid}{randomtimestamp}
        return $details;
    }

//    public function getReloadlist($id) {
//
//        $article = Reloadlist::find($id);
//
//        return response()->json($article);
//    }
//
//    public function saveReloadlist(Request $request) {
//
//        $article = Reloadlist::create($request->all());
//
//        return response()->json($article);
//    }
//
//    public function deleteReloadlist($id) {
//        $article = Reloadlist::find($id);
//
//        $article->delete();
//
//        return response()->json('success');
//    }
//
//    public function updateReloadlist(Request $request, $id) {
//        $article = Reloadlist::find($id);
//
//        $article->title = $request->input('title');
//        $article->content = $request->input('content');
//
//        $article->save();
//
//        return response()->json($article);
//    }
}
