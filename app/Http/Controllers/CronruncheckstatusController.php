<?php

namespace App\Http\Controllers;

use App\Checkstatustransaction;
use App\Reloadtransaction;
use App\User;
use App\Errorcode;
use DB;
use Log;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CronruncheckstatusController extends Controller {

    public function index(Request $request) {
            try {
//get pending txns
            $submittedTxns = Reloadtransaction::getSubmittedTxns(20);
            if ($submittedTxns) {
                foreach ($submittedTxns as $submittedTxn) {
                    $dateTxn = $submittedTxn->date_start;
                    $mobile_no = $submittedTxn->mobile_no;
                    $temp = explode(' ', $dateTxn);
                    $dateStart = $temp[0];
                    $dateTo = date('Y-m-d', strtotime($dateStart . ' +1 day'));

//route to celcom, mypay
                    $reload_class = '\\App\\Library\\' . $submittedTxn->class_name;
                    $class = new $reload_class();
                    Log::info('Run Check status ');
                    $class->check($submittedTxn->internal_ref_no, $dateStart, $dateTo,$mobile_no);
                }
            }
        } catch (Exception $ex) {
            Log::error('Run Check Status ' . $ex->getMessage());
        }
    }

}
