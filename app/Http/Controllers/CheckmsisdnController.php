<?php

namespace App\Http\Controllers;

use App\Checkmsisdn;
use App\Outgoingcheckmsisdntransaction;
use App\User;
use DB;
use Log;
use Symfony\Component\Console\Input\Input;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckmsisdnController extends Controller {

    public function index(Request $request) {
        try {

            $this->validate($request, [
                'mobile_no' => 'required',
                'reload_id' => 'required',
            ]);

            //get reload id class
            $checkMsisdnClass = Reloadlist::getReloadList($request->input('reload_id'));
            if ($checkMsisdnClass) {
                //store  txn into db
                $refNo = Checkmsisdn::store($request->input('mobile_no'),$request->input('reload_id'), $request->header('X-Authentication'), $request->ip());
                Log::info('Incoming check msisdn stored');
            } else {

                $response = [
                    'status' => $returnStatus = env('STATUS_FAILED', 2),
                    'mobile_no' => $request->input('mobile_no'),
                    'reference_no' => '',
                    'message' => 'Check MSISDN currently Not Available on this product'
                ];

                Log::info('API Response ' . $request->path() . ': ' . json_encode($response));

                return response()->json($response);

                exit();
            }

            if ($refNo) {

                //call api check mobile available/is prepaid number
                $checkClass = '\\App\\Library\\'.$checkMsisdnClass;
                $class = new $checkClass();
                $result = $class->checkMobileNumber($request->input('mobile_no'));
                Log::info('Runned check msisdn API ');
//store txn into db
                $retCode = Outgoingcheckmsisdntransaction::process($result, $refNo);
                Log::info('Outgoing check msisdn stored');

                //update incoming table
                Checkmsisdn::updateStatus($refNo, $retCode);
                Log::info('Incoming status updated');

                if ($retCode == 0) {
                    $returnStatus = env('STATUS_SUCCESS', 0); //failed
                } else if ($retCode != 0) {
                    $returnStatus = env('STATUS_FAILED', 2); //failed
                } else {
                    $returnStatus = env('STATUS_PENDING', 1); //failed
                }

                $response = [
                    'status' => $returnStatus,
                    'mobile_no' => $request->input('mobile_no'),
                    'reference_no' => $refNo,
                    'message' => ''
                ];

                Log::info('API Response ' . $request->path() . ': ' . json_encode($response));

                return response()->json($response);
            } else {
                echo 'Server Error: Unable to store msisdn check transaction';
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

}
