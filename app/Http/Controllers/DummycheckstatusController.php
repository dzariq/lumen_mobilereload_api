<?php

namespace App\Http\Controllers;

use App\Checkstatustransaction;
use App\Reloadtransaction;
use App\User;
use App\Errorcode;
use DB;
use Log;
use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DummycheckstatusController extends Controller {

    public function index(Request $request) {
        try {
//get pending txns
            $submittedTxns = Reloadtransaction::getPendingTxns(20);
            if ($submittedTxns) {
                foreach ($submittedTxns as $submittedTxn) {
                    $reloadTxn = Reloadtransaction::where('reference_no', '=', $submittedTxn->reference_no)
                            ->get();
                    $reloadTxn = $reloadTxn[0];
                    $reloadTxn->message = 'Dummy Success Response';
                    $reloadTxn->status = 0;
                    $reloadTxn->submit = 1;
                    $reloadTxn->save();
                    app('App\Http\Controllers\CallbackController')->index($submittedTxn->reference_no);
                }
            }
        } catch (Exception $ex) {
            Log::error('Run Check Status ' . $ex->getMessage());
        }
    }

}
