<?php

namespace App\Http\Controllers;

use App\Reloadlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReloadlistController extends Controller {

    public function index() {
        try {
            $articles = Reloadlist::select('reload_id', 'reload_amount', 'currency', 'provider.name as provider')
                    ->leftJoin('provider', 'provider.id', '=', 'reload_list.provider_id')
                    ->get();

            $data = array(
                'reload_list' => $articles
            );
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die;
        }


        return response()->json($data);
    }

//    public function getReloadlist($id) {
//
//        $article = Reloadlist::find($id);
//
//        return response()->json($article);
//    }
//
//    public function saveReloadlist(Request $request) {
//
//        $article = Reloadlist::create($request->all());
//
//        return response()->json($article);
//    }
//
//    public function deleteReloadlist($id) {
//        $article = Reloadlist::find($id);
//
//        $article->delete();
//
//        return response()->json('success');
//    }
//
//    public function updateReloadlist(Request $request, $id) {
//        $article = Reloadlist::find($id);
//
//        $article->title = $request->input('title');
//        $article->content = $request->input('content');
//
//        $article->save();
//
//        return response()->json($article);
//    }
}
