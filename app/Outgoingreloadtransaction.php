<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Reloadtransaction;
use App\Errorcode;
use App\Reloadsequence;
use Log;

class Outgoingreloadtransaction extends Model {

    protected $table = 'outgoing_reload_transaction';

    public static function store($gateway_id, $refNum, $xml_post_string, $response, $message, $txn_id, $error_code = NULL, $provider_id) {
        try {
            //get sequence number - which payment sequence currently - query by gateway id and provider id
            $sequence = Reloadsequence::getSequenceNumber($provider_id, $gateway_id);

            $store = new Outgoingreloadtransaction();
            $store->reference_no = $refNum;
            $store->post_data = $xml_post_string;
            $store->response_data = $response;
            $store->message = $message;
            $store->transaction_id = $txn_id;
            $store->error_code = $error_code;
            $store->sequence = $sequence;
            $store->save();

            $reloadTxn = Reloadtransaction::where('reference_no', '=', $refNum)
                    ->get();
            $reloadTxn = $reloadTxn[0];

            //if submit reload success, else..will retry by cronjob
            if ($txn_id != '') {
                $reloadTxn->submit = 1;
            }

            $reloadTxn->internal_ref_no = $txn_id;

            if ($error_code) {

                //check if reload next sequence exists
                $next_sequence = $sequence + 1;


                //if exists next seq of reload..try ..else...accept it as error reload
                $next_gateway = Reloadsequence::getNextGateway($provider_id, $next_sequence);
                if ($next_gateway) {
                    $reload_class = '\\App\\Library\\' . $next_gateway->class_name;
                    $class = new $reload_class();
                    Log::info('Run Reload ');
                    $class->reload($next_gateway->gateway_id, $reloadTxn->mobile_no, $reloadTxn->reload_amount, $next_gateway->provider_code, $provider_id, $refNum);
                } else {
                    $reloadTxn->status = 2;
                    //get mapped error code
                    $mappedCode = Errorcode::getMappedCode($gateway_id, $error_code);
                    if ($mappedCode) {
                        $reloadTxn->error_code = $mappedCode->custom_error_code;
                        $reloadTxn->message = $mappedCode->description;
                    }
                }
            }
            $reloadTxn->save();
        } catch (Exception $ex) {
            Log::error('outgoing reload txn  ' . $ex->getMessage());
        }
    }

}
