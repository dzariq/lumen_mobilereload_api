<?php

namespace App;

use App\Userbalancetransaction;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Reloadtransaction extends Model {

    protected $table = 'incoming_reload_transaction';

    public static function processTransaction($data, $refNo, $details, $ip) {
        try {
            if ($details['status']) {
                //deduct balance
                $userAccount = new Userbalancetransaction;
                $userAccount->user_id = $details['user_id'];
                $userAccount->balance_before = $details['user_balance'];
                $userAccount->balance = '-' . $details['amount'];
                $userAccount->event = 'mobilereload';
                $userAccount->reference_no = $refNo;
                $userAccount->save();

                $user = User::find($details['user_id']);
                $user->balance = $user->balance - $details['amount'];
                $user->save();
            }
            //save txn
            $reload_transaction = new Reloadtransaction;
            $reload_transaction->reload_id = $data['reload_id'];
            $reload_transaction->user_id = $details['user_id'];
            $reload_transaction->tx_id = $data['tx_id'];
            $reload_transaction->tx_datetime = $data['tx_datetime'];
            $reload_transaction->mobile_no = $data['mobile_no'];
            $reload_transaction->reference_no = $refNo;
            $reload_transaction->ip_address = $ip;
            $reload_transaction->message = $details['message'];
            $reload_transaction->status = !$details['status'] ? 2 : 1;
            $reload_transaction->error_code = $details['error_code'];
            $reload_transaction->save();
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function getCallbackUrl($refNo = null) {
        $query = Reloadtransaction::select('*')
                ->leftJoin('users', 'users.id', '=', 'incoming_reload_transaction.user_id')
                ->where('reference_no', '=', $refNo)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query[0]->callback_url;
        }
    }

    public static function getUserId($refNo = null) {
        $query = Reloadtransaction::select('*')
                ->leftJoin('users', 'users.id', '=', 'incoming_reload_transaction.user_id')
                ->where('reference_no', '=', $refNo)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query[0]->user_id;
        }
    }

    public static function getTxn($refNo = null) {
        $query = Reloadtransaction::select('*')
                ->where('reference_no', '=', $refNo)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query[0];
        }
    }

    public static function getPendingTxns($limit = 1) {
        $query = Reloadtransaction::select('reference_no','ereload_id', 'gateway.id as gateway_id', 'gateway.name as gateway_name', 'class_name', 'mobile_no', 'provider_code', 'reload_list.provider_id', 'reload_amount')
                ->leftJoin('reload_list', 'reload_list.reload_id', '=', 'incoming_reload_transaction.reload_id')
                ->leftJoin('provider', 'provider.id', '=', 'reload_list.provider_id')
                ->leftJoin('reload_gw_sequence', 'reload_gw_sequence.provider_id', '=', 'reload_list.provider_id')
                ->leftJoin('gateway', 'gateway.id', '=', 'reload_gw_sequence.gateway_id')
                ->where('submit', '=', '0')
                ->where('sort_order', '=', '1')
                ->take($limit)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query;
        }
    }

    public static function getSubmittedTxns($limit = 1) {
        $query = Reloadtransaction::select('mobile_no','incoming_reload_transaction.updated_at as date_start', 'internal_ref_no', 'class_name')
                ->leftJoin('reload_list', 'reload_list.reload_id', '=', 'incoming_reload_transaction.reload_id')
                ->leftJoin('reload_gw_sequence', 'reload_gw_sequence.provider_id', '=', 'reload_list.provider_id')
                ->leftJoin('gateway', 'gateway.id', '=', 'reload_gw_sequence.gateway_id')
                ->where('submit', '=', '1')
                ->where('sort_order', '=', '1')
                ->where('incoming_reload_transaction.status', '=', '1')
                ->take($limit)
                ->get();

        if (count($query) == 0) {
            return false;
        } else {
            return $query;
        }
    }

}
