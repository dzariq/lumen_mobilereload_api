<?php

namespace App;

use App\Userbalancetransaction;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Checkmsisdn extends Model {

    protected $table = 'incoming_checkmsisdn_transaction';

    public static function store($mobile_no,$reload_id, $auth, $ip) {
        try {

            $userDetails = User::getUserByHeader($auth);

            $msisdn = new Checkmsisdn;

            $msisdn->mobile_no = $mobile_no;
            $msisdn->user_id = $userDetails->id;
            $msisdn->ip_address = $ip;
            $msisdn->reload_id = $reload_id;
            $msisdn->reference_no = $random = 'MC_' . $userDetails->id . time() . rand(10 * 45, 100 * 98);
            $msisdn->save();

            return $msisdn->reference_no;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function updateStatus($refNo, $retCode) {
        try {
            $res = Checkmsisdn::where('reference_no', '=', $refNo)
                    ->get();

            $incoming = $res[0];
            if ($retCode == 0) {
                $incoming->status = 'VALID';
            } else {
                $incoming->status = 'INVALID';
            }
            
            $incoming->save();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

}
